import tempfile

import mock
from shapely.geometry import Polygon as SPolygon
from shapely.affinity import translate
from inspire_isles.compute_distances import main
from inspire_isles.title import Title, Polygon
from inspire_isles.csv_util import load_csv


def my_load_ros_inspire():
    geom_1 = SPolygon([(0, 0), (1, 0), (1, 1), (0, 1), (0, 0)])
    titles = [
        Title("t1", [
            Polygon(polygon_id=1, geometry=geom_1),
            Polygon(polygon_id=2, geometry=translate(geom_1, 1.1, 1))
        ]),
        Title("t2", [
            Polygon(polygon_id=3, geometry=translate(geom_1, 2.1, 0))
        ]),
        Title("t3", [
            Polygon(polygon_id=4, geometry=translate(geom_1, 1.5, 0))
        ]),
        Title("t4", [
            Polygon(polygon_id=5, geometry=translate(geom_1, 10, 1))
        ])
    ]
    title_dict = {
        title.title_id: title
        for title in titles
    }
    polygon_dict = {
        polygon.polygon_id: polygon
        for title in titles
        for polygon in title.polygons
    }
    return title_dict, polygon_dict


def test_compute_distances():
    tmp_distances = tempfile.NamedTemporaryFile()
    tmp_title_info = tempfile.NamedTemporaryFile()
    args = [
        "my-script.py",
        "--output-distances", tmp_distances.name,
        "--output-title-info", tmp_title_info.name,
        "input_file"
    ]
    load_inspire = mock.MagicMock(return_value=my_load_ros_inspire())
    with mock.patch("inspire_isles.ros_inspire.load_ros_inspire", load_inspire):
        with mock.patch("sys.argv", args):
            main()

    load_inspire.assert_called_once_with(["input_file"])

    expected_distances = {
        (1, 2): 0.1,
        (1, 3): 1.1,
        (1, 4): 0.5,
        (2, 3): 0,
        (2, 4): 0,
        (3, 4): 0
    }

    distances = {
        (int(r["polygon_1"]), int(r["polygon_2"])): float(r["distance"])
        for r in load_csv(tmp_distances.name, ["polygon_1", "polygon_2", "distance"])
    }

    assert len(distances) == len(expected_distances)

    for k, v in expected_distances.items():
        assert abs(distances[k] - v) < 0.001

    expected_title_info = [
        dict(title_id="t1", polygon_id="1"),
        dict(title_id="t1", polygon_id="2"),
        dict(title_id="t2", polygon_id="3"),
        dict(title_id="t3", polygon_id="4"),
        dict(title_id="t4", polygon_id="5"),
    ]

    title_info = list(load_csv(tmp_title_info.name, ["title_id", "polygon_id"]))
    assert expected_title_info == title_info
