from inspire_isles.ros_inspire import load_ros_inspire, _load_titles
from inspire_isles.title import Title, Polygon
import mock
import shapely.geometry


def test_ros_inspire():
    geom_1 = mock.MagicMock()
    geom_2 = mock.MagicMock()
    titles = [
        Title("my_title", [Polygon(3, geom_1), Polygon(4, geom_2)])
    ]
    with mock.patch("inspire_isles.ros_inspire._load_titles", return_value=titles):
        title_dict, polygon_dict = load_ros_inspire(mock.MagicMock())

    print(polygon_dict)
    assert title_dict == {
        "my_title": titles[0]
    }
    assert polygon_dict == {
        3: Polygon(3, geom_1),
        4: Polygon(4, geom_2)
    }


def test_load_titles():
    features = [{
        "properties": {
            "inspireid": "title_1"
        },
        "geometry": {"type": "Polygon",
                     "coordinates": [
                         [[100.0, 0.0], [101.0, 0.0], [101.0, 1.0],
                          [100.0, 1.0], [100.0, 0.0]]]
                     }
    }, {
        "properties": {
            "inspireid": "title_2"
        },
        "geometry": {"type": "MultiPolygon",
                     "coordinates": [
                         [[[102.0, 2.0], [103.0, 2.0], [103.0, 3.0], [102.0, 3.0],
                          [102.0, 2.0]]],
                         [[[100.0, 0.0], [101.0, 0.0], [101.0, 1.0],
                          [100.0, 1.0], [100.0, 0.0]]]]}
    }]

    fiona_open = mock.MagicMock()
    fiona_open.__enter__.return_value = features
    with mock.patch("fiona.open", return_value=fiona_open):
        titles = list(_load_titles([mock.MagicMock()]))

    polygon_1 = shapely.geometry.Polygon([
        [100.0, 0.0], [101.0, 0.0], [101.0, 1.0], [100.0, 1.0], [100.0, 0.0]
    ])
    polygon_2 = shapely.geometry.Polygon([
        [102.0, 2.0], [103.0, 2.0], [103.0, 3.0], [102.0, 3.0], [102.0, 2.0]
    ])
    polygon_3 = polygon_1

    assert titles == [
        Title(title_id='title_1',
              polygons=[
                  Polygon(polygon_id=0, geometry=polygon_1)
              ]),
        Title(title_id='title_2', polygons=[
            Polygon(polygon_id=1, geometry=polygon_2),
            Polygon(polygon_id=2, geometry=polygon_3)])
    ]
