import pytest

from inspire_isles.distances import get_all_distances
from inspire_isles.title import Polygon
from shapely.geometry import box


def as_dict(polygons):
    return {
        index: Polygon(index, geom)
        for index, geom in enumerate(polygons)
    }


# Things that are 0.5m from one-another
TEST_CASES_05 = [
    [box(0, 0, 1, 1), box(1.5, 0, 2.5, 1)],
    [box(0, 0, 1, 1), box(0, 1.5, 1, 2.5)],
    [box(0, 0, 2, 12), box(0.5, 0.5, 1, 1)],
    [box(0, 0, 1, 1), box(1.5, 1, 2, 2)],
]


@pytest.mark.parametrize("geometries", TEST_CASES_05)
def test_two(geometries):
    polygon_dict = as_dict(geometries)
    expected = [
        (polygon_dict[0], polygon_dict[1], 0.5)
    ]

    for buffer_size in [0.5, 0.6]:
        distances = list(
            get_all_distances(polygon_dict, buffer_size)
        )

        assert expected == distances


@pytest.mark.parametrize("geometries", TEST_CASES_05)
def test_no_match(geometries):
    for buffer_size in [0.3, 0.499]:
        distances = list(
            get_all_distances(as_dict(geometries), 0.49)
        )

        assert [] == distances
