#!/usr/bin/env python

import setuptools


with open("README.md", "r") as fh:
    long_description = fh.read()


setuptools.setup(
    name='scotland-inspire-isles',
    version='0.1.0',
    description='Encode and decode British National Grid References',
    long_description=long_description,
    long_description_content_type="text/markdown",
    author='JB Robertson',
    author_email='jbr@freeshell.org',
    url='https://gitlab.com/jbrobertson/scotland-inspire-isles',
    packages=['inspire_isles'],
    python_requires='>=3.8',
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    install_requires=[
        "rtree==0.9.7",
        "fiona==1.8.20",
        "shapely==1.7.1",
        "networkx==2.6.2",
    ]
)
