"""
This is tailored for the *_bng.shp files made available by
Registers of Scotland at https://ros.locationcentre.co.uk/inspire/
(as of September 2021).

"""

import logging
from typing import Iterable, List, Dict, Tuple

import fiona
import shapely.geometry

from .title import Title, Polygon


def _load_titles(files: List[str]) -> Iterable[Title]:
    logging.info("Loading polygons")
    polygon_id = 0
    nb_titles = 0

    for path in files:
        logging.info(f"Loading {path}")
        with fiona.open(path) as layer:
            for feature in layer:
                nb_titles += 1
                geometry = shapely.geometry.shape(feature["geometry"])
                title_id = feature["properties"]["inspireid"]
                if geometry.geom_type == "Polygon":
                    geometries = [geometry]
                elif geometry.geom_type == "MultiPolygon":
                    geometries = list(geometry)
                else:
                    raise Exception(f"Unsupported geometry type: {geometry.geom_type}")

                polygons = []
                for p in geometries:
                    poly = Polygon(
                        polygon_id=polygon_id,
                        geometry=p,
                    )
                    polygon_id += 1
                    polygons.append(poly)

                if nb_titles % 10000 == 0:
                    logging.info(f"Loaded {nb_titles} titles, {polygon_id} polygons so far")
                yield Title(title_id, polygons)

    logging.info(f"Loaded {nb_titles} titles, {polygon_id + 1} polygons")


def load_ros_inspire(files: List[str]) -> Tuple[Dict[str, Title],
                                                Dict[int, Polygon]]:
    titles = list(_load_titles(files))
    polygon_dict = {
        polygon.polygon_id: polygon
        for title in titles
        for polygon in title.polygons
    }
    title_dict = {
        title.title_id: title
        for title in titles
    }
    return title_dict, polygon_dict
