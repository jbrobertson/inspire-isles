import logging
from typing import Dict, Iterable, Tuple

import rtree.index

from inspire_isles.title import Polygon


def get_all_distances(polygon_dict: Dict[int, Polygon],
                      buffer: float) -> Iterable[Tuple[Polygon, Polygon, float]]:

    index = create_index(polygon_dict.values())
    nb_distances = 0
    nb_polygons = 0

    for polygon in polygon_dict.values():
        nb_polygons += 1
        boundary = polygon.geometry.boundary
        xmin, ymin, xmax, ymax = boundary.bounds
        search_bounds = (
            xmin - buffer, ymin - buffer,
            xmax + buffer, ymax + buffer
        )

        for intersected_index in index.intersection(search_bounds):
            if intersected_index > polygon.polygon_id:
                intersected_polygon = polygon_dict[intersected_index]
                distance = intersected_polygon.geometry.boundary.distance(boundary)
                if distance <= buffer:
                    nb_distances += 1
                    yield polygon, intersected_polygon, distance

        if nb_polygons % 10000 == 0:
            logging.info(f"Computed {nb_distances} distances for {nb_polygons} polygons so far")

    logging.info(f"Computed {nb_distances} distances for {nb_polygons} polygons")


def create_index(polygons: Iterable[Polygon]) -> rtree.index.Index:
    logging.info("Indexing")

    def iterable():
        nb_indexed = 0
        for p in polygons:
            nb_indexed += 1
            yield p.polygon_id, p.geometry.bounds, None
            if nb_indexed % 10000 == 0:
                logging.info(f"Indexed {nb_indexed} polygons so far")
        logging.info(f"Indexed {nb_indexed} polygons")

    return rtree.index.Index(iterable())
