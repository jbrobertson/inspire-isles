import argparse
import logging

from .distances import get_all_distances
from . import ros_inspire
from . import csv_util


def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("--output-distances", required=True, type=str, help="Output CSV file")
    parser.add_argument("--output-title-info", required=True, type=str, help="Output CSV file")
    parser.add_argument("--buffer-size", default=2, type=float, help="Buffer size")
    parser.add_argument('input_file', nargs='+', type=str, help="Input INSPIRE files")
    return parser


def main():
    args = get_parser().parse_args()
    logging.basicConfig(level="INFO", format='%(asctime)-15s %(message)s')

    title_dict, polygon_dict = ros_inspire.load_ros_inspire(args.input_file)
    distance_iterator = get_all_distances(polygon_dict, args.buffer_size)

    logging.info(f"Writing title information to {args.output_title_info}")

    csv_util.write_csv(
        args.output_title_info, ["title_id", "polygon_id"],
        (dict(title_id=title.title_id, polygon_id=polygon.polygon_id)
         for title in title_dict.values()
         for polygon in title.polygons)
    )

    logging.info(f"Writing distances to {args.output_distances}")
    csv_util.write_csv(
        args.output_distances,
        ["polygon_1", "polygon_2", "distance"],
        (dict(polygon_1=p1.polygon_id, polygon_2=p2.polygon_id,
              distance=distance)
         for p1, p2, distance in distance_iterator)
    )


if __name__ == "__main__":
    main()
