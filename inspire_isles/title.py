from typing import NamedTuple, List, Any

import shapely.geometry


class Polygon(NamedTuple):
    polygon_id: Any
    geometry: shapely.geometry.Polygon


class Title(NamedTuple):
    title_id: Any
    polygons: List[Polygon]
