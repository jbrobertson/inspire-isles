import csv
import typing
from typing import List


def load_csv(path: str, required_headers: List[str]):
    with open(path) as input_fd:
        dialect = csv.Sniffer().sniff(input_fd.read(3000))
        input_fd.seek(0)
        reader = csv.DictReader(input_fd, dialect=dialect)

        if not set(reader.fieldnames).issuperset(required_headers):
            raise Exception(f"Expected headers {required_headers}, got {reader.fieldnames}")

        yield from reader


def write_csv(path: str, headers: List[str], items: typing.Generator):
    with open(path, "w") as output_fd:
        writer = csv.DictWriter(output_fd, fieldnames=headers)
        writer.writeheader()
        for item in items:
            writer.writerow(item)
