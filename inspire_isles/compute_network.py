import argparse
import logging
from typing import List, Dict

import networkx
import os
from .csv_util import load_csv


def _load_distance_files(distance_files: List[str]):
    for path in distance_files:
        logging.info(f"Loading distance info from {path}")
        for record in load_csv(path, required_headers=["polygon_1", "polygon_2"]):
            yield record["polygon_1"], record["polygon_2"]


def _load_title_info_files(files: List[str]):
    titles = set()
    polygons = set()
    relations = set()

    for path in files:
        logging.info(f"Loading title info from {path}")
        for record in load_csv(path, required_headers=["title_id", "polygon_id"]):
            title_id = record["title_id"]
            polygon_id = record["polygon_id"]
            polygons.add(polygon_id)
            titles.add(title_id)
            relations.add((title_id, polygon_id))

    return titles, polygons, relations


def load_network(title_info_files: List[str], distance_files: List[str]) -> networkx.Graph:
    network = networkx.Graph()

    logging.info("Loading title-polygon relations")
    titles, polygons, relations = _load_title_info_files(title_info_files)
    logging.info("Adding titles")
    network.add_nodes_from(titles, node_type="title")
    logging.info("Adding polygons")
    network.add_nodes_from(polygons, node_type="polygon")
    logging.info("Adding title-polygon relations")
    network.add_edges_from(relations)

    logging.info("Adding distance relations")
    network.add_edges_from(
        _load_distance_files(distance_files),
        relation_type="proximity"
    )

    return network


def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("--output-dir", required=True, type=str, help="Output directory")
    parser.add_argument('--polygon-rel', nargs='+', type=str, help="Input CSV files")
    parser.add_argument('--tp-rel', nargs='+', type=str, help="Input CSV files")
    return parser


def output_components(network: networkx.Graph,
                      component_elements_dict: Dict[int, set],
                      output_base_dir: str):
    nb_components = 0
    logging.info(f"Writting components to {output_base_dir}")
    for component_id, component_elements in component_elements_dict.items():

        nb_components += 1
        component = networkx.induced_subgraph(network, component_elements)
        output_dir = os.path.join(output_base_dir, f'{component_id % 1024}')
        output_path = os.path.join(output_dir, f"{component_id}.gml")
        os.makedirs(output_dir, exist_ok=True)

        networkx.write_gml(component, path=output_path)

        if nb_components % 1000 == 0:
            logging.info(f"Written {nb_components} components so far")

    logging.info(f"Written {nb_components} components")


def interesting_components(network: networkx.Graph,
                           component_elements_dict: Dict[int, set]):
    for component_id, component_elements in component_elements_dict.items():
        subgraph = networkx.induced_subgraph(network, component_elements)
        polygons = [
            x
            for x, y in subgraph.nodes(data=True)
            if y["node_type"] == "polygon"
        ]
        polygon_graph = networkx.induced_subgraph(network, polygons)
        polygon_components = list(networkx.connected_components(polygon_graph))
        if len(polygon_components) > 1:
            is_interesting = [
                polyc for polyc in polygon_components
                if len(polyc) > 3
            ]
            if len(is_interesting) > 2:
                logging.info(f"Component {component_id}: "
                             f"interesting pc={len(polygon_components)} "
                             f"polygons={len(polygons)} "
                             f"components={len(component_elements)}")


def main():
    args = get_parser().parse_args()
    logging.basicConfig(level="INFO", format='%(asctime)-15s %(message)s')

    network = load_network(args.tp_rel, args.polygon_rel)

    component_dict = {
        component_id: component
        for component_id, component in enumerate(networkx.connected_components(network))
    }

    output_components(network, component_dict, args.output_dir)
    interesting_components(network, component_dict)


if __name__ == "__main__":
    main()
